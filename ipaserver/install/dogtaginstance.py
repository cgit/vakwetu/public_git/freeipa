# Authors: Ade Lee <alee@redhat.com>
#
# Copyright (C) 2014  Red Hat
# see file 'COPYING' for use and warranty information
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import base64
import os
import shutil
import tempfile
import traceback

from pki.client import PKIConnection
import pki.system

from ipapython import certmonger
from ipapython import dogtag
from ipapython import ipaldap
from ipapython import ipautil
from ipapython import services as ipaservices
from ipapython.dn import DN
from ipaserver.install import service
from ipaserver.install import installutils
from ipaserver.install.installutils import stopped_service
from ipapython.ipa_log_manager import *

HTTPD_CONFD = "/etc/httpd/conf.d/"
DEFAULT_DSPORT = dogtag.install_constants.DS_PORT

PKI_USER = "pkiuser"
PKI_DS_USER = dogtag.install_constants.DS_USER


def check_inst(subsystem):
    """
    Validate that the appropriate dogtag/RHCS packages have been installed.
    """

    # Check for a couple of binaries we need
    if not os.path.exists(dogtag.install_constants.SPAWN_BINARY):
        return False
    if not os.path.exists(dogtag.install_constants.DESTROY_BINARY):
        return False

    if not os.path.exists('/usr/share/pki/%s/conf/server.xml' % subsystem):
        return False

    return True


def get_security_domain():
    """
    Get the security domain from the REST interface on the local Dogtag CA
    This function will succeed if the local dogtag CA is up.
    """
    connection = PKIConnection()
    domain_client = pki.system.SecurityDomainClient(connection)
    info = domain_client.get_security_domain_info()
    return info


def is_installing_replica(sys_type):
    """
    We expect only one of each type of Dogtag subsystem in an IPA deployment.
    That means that if a subsystem of the specified type has already been
    deployed - and therefore appears in the security domain - then we must be
    installing a replica.
    """
    info = get_security_domain()
    try:
        sys_list = info.systems[sys_type]
        return len(sys_list.hosts) > 0
    except KeyError:
        return False


class DogtagInstance(service.Service):
    """
    This is the base class for a Dogtag 10+ instance, which uses a
    shared tomcat instance and DS to host the relevant subsystems.

    It contains functions that will be common to installations of the
    CA, KRA, and eventually TKS and TPS.
    """

    ADMIN_CERT_PATH = '/root/.dogtag/pki-tomcat/ca_admin.cert'
    AGENT_P12_PATH = '/root/ca-agent.p12'

    def __init__(self, realm, subsystem, service_desc, dogtag_constants=None):
        if dogtag_constants is None:
            dogtag_constants = dogtag.configured_constants()

        service.Service.__init__(self,
                                 '%sd' % dogtag_constants.PKI_INSTANCE_NAME,
                                 service_desc=service_desc)

        self.dogtag_constants = dogtag_constants
        self.realm = realm
        self.dm_password = None
        self.admin_password = None
        self.fqdn = None
        self.domain = None
        self.pkcs12_info = None
        self.clone = False

        self.basedn = DN(('o', 'ipa%s' % subsystem.lower()))
        self.admin_user = DN(('uid', 'admin'), ('ou', 'people'), ('o', 'ipaca'))
        self.agent_db = tempfile.mkdtemp(prefix="tmp-")
        self.ds_port = DEFAULT_DSPORT
        self.server_root = dogtag_constants.SERVER_ROOT
        self.subsystem = subsystem
        self.security_domain_name = "IPA"
        self.tracking_nicknames = None

        # replication parameters
        self.master_host = None
        self.master_replication_port = None
        self.subject_base = None

    def __del__(self):
        shutil.rmtree(self.agent_db, ignore_errors=True)

    def is_installed(self):
        """
        Determine if subsystem instance has been installed.

        Returns True/False
        """
        return os.path.exists(os.path.join(
            self.server_root, self.dogtag_constants.PKI_INSTANCE_NAME,
            self.subsystem.lower()))

    def spawn_instance(self, cfg_file, nolog_list=None):
        """
        Create and configure a new Dogtag instance using pkispawn.
        Passes in a configuration file with IPA-specific
        parameters.
        """
        subsystem = self.subsystem

        # Define the things we don't want logged
        if nolog_list is None:
            nolog_list = []
        nolog_list.extend([self.admin_password, self.dm_password])
        nolog = tuple(nolog_list)

        args = ["/usr/sbin/pkispawn",
                "-s", subsystem,
                "-f", cfg_file]

        with open(cfg_file) as f:
            root_logger.debug(
                'Contents of pkispawn configuration file (%s):\n%s' %
                (cfg_file, ipautil.nolog_replace(f.read(), nolog)))

        try:
            ipautil.run(args, nolog=nolog)
        except ipautil.CalledProcessError, e:
            root_logger.critical("failed to configure %s instance %s" %
                                 (subsystem, e))
            raise RuntimeError('Configuration of %s failed' % subsystem)

    def enable(self):
        self.backup_state("enabled", self.is_enabled())

    # noinspection PyBroadException
    def restart_instance(self):
        try:
            self.restart(self.dogtag_constants.PKI_INSTANCE_NAME)
        except Exception:
            root_logger.debug(traceback.format_exc())
            root_logger.critical(
                "Failed to restart the Dogtag instance."
                "See the installation log for details.")

    # noinspection PyBroadException
    def start_instance(self):
        try:
            self.start(self.dogtag_constants.PKI_INSTANCE_NAME)
        except Exception:
            root_logger.debug(traceback.format_exc())
            root_logger.critical(
                "Failed to restart the Dogtag instance."
                "See the installation log for details.")

    # noinspection PyBroadException
    def stop_instance(self):
        try:
            self.stop(self.dogtag_constants.PKI_INSTANCE_NAME)
        except Exception:
            root_logger.debug(traceback.format_exc())
            root_logger.critical(
                "Failed to restart the Dogtag instance."
                "See the installation log for details.")

    def enable_client_auth_to_db(self, config):
        """
        Enable client auth connection to the internal db.
        Path to CS.cfg config file passed in.
        """

        with stopped_service(
                self.dogtag_constants.SERVICE_NAME,
                instance_name=self.dogtag_constants.PKI_INSTANCE_NAME):
            installutils.set_directive(
                config,
                'authz.instance.DirAclAuthz.ldap.ldapauth.authtype',
                'SslClientAuth', quotes=False, separator='=')
            installutils.set_directive(
                config,
                'authz.instance.DirAclAuthz.ldap.ldapauth.bindDN',
                'uid=pkidbuser,ou=people,o=ipaca', quotes=False, separator='=')
            installutils.set_directive(
                config,
                'authz.instance.DirAclAuthz.ldap.ldapauth.clientCertNickname',
                'subsystemCert cert-pki-ca', quotes=False, separator='=')
            installutils.set_directive(
                config,
                'authz.instance.DirAclAuthz.ldap.ldapconn.port',
                str(dogtag.install_constants.DS_SECURE_PORT),
                quotes=False, separator='=')
            installutils.set_directive(
                config,
                'authz.instance.DirAclAuthz.ldap.ldapconn.secureConn',
                'true', quotes=False, separator='=')

            installutils.set_directive(
                config,
                'internaldb.ldapauth.authtype',
                'SslClientAuth', quotes=False, separator='=')

            installutils.set_directive(
                config,
                'internaldb.ldapauth.bindDN',
                'uid=pkidbuser,ou=people,o=ipaca', quotes=False, separator='=')
            installutils.set_directive(
                config,
                'internaldb.ldapauth.clientCertNickname',
                'subsystemCert cert-pki-ca', quotes=False, separator='=')
            installutils.set_directive(
                config,
                'internaldb.ldapconn.port',
                str(dogtag.install_constants.DS_SECURE_PORT),
                quotes=False, separator='=')
            installutils.set_directive(
                config,
                'internaldb.ldapconn.secureConn', 'true', quotes=False,
                separator='=')

    def uninstall(self):
        if self.is_installed():
            self.print_msg("Unconfiguring %s" % self.subsystem)

        try:
            ipautil.run(["/usr/sbin/pkidestroy", "-i",
                         self.dogtag_constants.PKI_INSTANCE_NAME,
                         "-s", self.subsystem])
        except ipautil.CalledProcessError, e:
            root_logger.critical("failed to uninstall %s instance %s"
                                 % (self.subsystem, e))

    def http_proxy(self):
        """ Update the http proxy file  """
        template_filename = ipautil.SHARE_DIR + "ipa-pki-proxy.conf"
        sub_dict = dict(
            DOGTAG_PORT=self.dogtag_constants.AJP_PORT,
            CLONE='' if self.clone else '#',
            FQDN=self.fqdn,
        )
        template = ipautil.template_file(template_filename, sub_dict)
        with open(HTTPD_CONFD + "ipa-pki-proxy.conf", "w") as fd:
            fd.write(template)

    def __get_pin(self):
        try:
            return certmonger.get_pin('internal',
                                      dogtag_constants=self.dogtag_constants)
        except IOError, e:
            root_logger.debug(
                'Unable to determine PIN for the Dogtag instance: %s' % str(e))
            raise RuntimeError(e)

    def configure_renewal(self, nicknames=None):
        """ Configure certmonger to renew system certs

        @param nicknames: list of nicknames
        """
        cmonger = ipaservices.knownservices.certmonger
        cmonger.enable()
        ipaservices.knownservices.messagebus.start()
        cmonger.start()

        pin = self.__get_pin()

        if nicknames is None:
            nicknames = self.tracking_nicknames

        for nickname in nicknames:
            try:
                certmonger.dogtag_start_tracking(
                    ca='dogtag-ipa-ca-renew-agent',
                    nickname=nickname,
                    pin=pin,
                    pinfile=None,
                    secdir=self.dogtag_constants.ALIAS_DIR,
                    pre_command='stop_pkicad',
                    post_command='renew_ca_cert "%s"' % nickname)
            except (ipautil.CalledProcessError, RuntimeError), e:
                root_logger.error(
                    "certmonger failed to start tracking certificate: %s" %
                    str(e))

    def stop_tracking_certificates(self, dogtag_constants, nicknames=None):
        """Stop tracking our certificates. Called on uninstall.
        """
        cmonger = ipaservices.knownservices.certmonger
        ipaservices.knownservices.messagebus.start()
        cmonger.start()

        if nicknames is None:
            nicknames = self.tracking_nicknames

        for nickname in nicknames:
            try:
                certmonger.stop_tracking(
                    dogtag_constants.ALIAS_DIR, nickname=nickname)
            except (ipautil.CalledProcessError, RuntimeError), e:
                root_logger.error(
                    "certmonger failed to stop tracking certificate: %s"
                    % str(e))

        cmonger.stop()

    @staticmethod
    def update_cert_cs_cfg(nickname, cert, directives, cs_cfg,
                           dogtag_constants=None):
        """
        When renewing a Dogtag subsystem certificate the configuration file
        needs to get the new certificate as well.

        nickname is one of the known nicknames.
        cert is a DER-encoded certificate.
        directives is the list of directives to be updated for the subsystem
        cs_cfg is the path to the CS.cfg file
        """

        if dogtag_constants is None:
            dogtag_constants = dogtag.configured_constants()

        with stopped_service(dogtag_constants.SERVICE_NAME,
                             instance_name=dogtag_constants.PKI_INSTANCE_NAME):
            installutils.set_directive(
                cs_cfg,
                directives[nickname],
                base64.b64encode(cert),
                quotes=False,
                separator='=')

    def get_admin_cert(self):
        """
        Get the certificate for the admin user by checking the ldap entry
        for the user
        """
        root_logger.debug('Trying to find the certificate for the admin user')
        conn = None

        try:
            conn = ipaldap.IPAdmin(self.fqdn, self.ds_port)
            conn.do_simple_bind(
                DN(('cn', 'Directory Manager')),
                self.dm_password)

            entry_attrs = conn.get_entry(self.admin_user, ['usercertificate'])
            admin_cert = entry_attrs.get('usercertificate')[0]
        finally:
            if conn is not None:
                conn.unbind()

        return base64.b64encode(admin_cert)
